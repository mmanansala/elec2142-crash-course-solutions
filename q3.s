fib
        stmfd sp!, {v1-v2, lr}
        cmp a1, #1
        movlt a1, #0
        ble end_fib
        mov v1, a1
        sub a1, a1, #1
        bl fib          ; fib(n-1)
        mov v2, a1
        sub a1, v1, #2
        bl fib          ; fib(n-2)
        add a1, a1, v2  ; fib(n-1) + fib(n-2)
end_fib ldmfd sp!, {v1-v2, lr}
        mov pc, lr