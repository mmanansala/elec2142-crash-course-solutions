lowerfy
        mov a3, #0 ; a3 = n = 0
loop    
        ldrsb a4, [a1], #1 ; load src[i] into a4 and increment address by 1
        cmp a4, #0 ; check if 0
        beq end_loop
        cmp a4, 'A' ; check if capital
        blt store_dest
        cmp a4, 'Z'
        bgt store_dest
        add a4, a4, #32 ; ch = ch - 'A' + 'a'
        add a3, a3, #1 ; n++
store_dest
        strsb a4, [a2], #1 ; dest[i] = ch
        b loop
end_loop
        strsb a4, [a2], #1 ; dest[i] = '\0'
        mov a1, a3 ; return n
        mov pc, lr