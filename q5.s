rmOdd
    stmfd sp!, {v1-v2}
    mov v1, #0 ; v1 = i = 0
    mov v2, #0 ; v2 = j = 0
loop
    cmp v1, a2 ; check i < n
    bge end_loop
    ldr a4, [a1, v1, lsl #2] ; load src[i]
    tst a4, #1 ; check if last bit is 1
    bne loop_inc ; if non-zero result, last bit is 1 and number is odd
    ; otherwise number is even
    str a4, [v3, v2, lsl #2] ; dest[j] = src[j]
    add v2, v2, #1 ; j++
loop_inc
    add v1, v1, #1 ; i++
    b loop
end_loop
    mov a1, v2 ; return j
    ldmfd sp!, {v1-v2}
    mov pc, lr